package db;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

import javax.inject.Inject;

public class Mongo {

    public MongoClient mongoClient;

    private static Mongo mongo;

    @Inject
    private Mongo() {

        mongoClient = new MongoClient();

    }

    public static Mongo getInstance() {
        mongo = mongo == null ? new Mongo() : mongo;
        return mongo;
    }

    public MongoDatabase getDB(String database) {
        return mongoClient.getDatabase(database);
    }

}
