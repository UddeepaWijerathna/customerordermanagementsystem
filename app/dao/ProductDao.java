package dao;

import bo.Product;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public interface ProductDao {

    public List<Object> getAllProducts();
    public Product getProductById(String productId);
    public void addProduct(JsonNode product);
    public void updateProduct(JsonNode product);
    public void deleteProduct(String productId);
}
