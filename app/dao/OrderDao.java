package dao;

import bo.Order;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public interface OrderDao {
    public List<Object> getAllOrders();
    public Order getOrderById(String orderId);
    public void addOrder(JsonNode order);
    public void updateOrder(JsonNode order);
    public void deleteOrder(String orderId);

}
