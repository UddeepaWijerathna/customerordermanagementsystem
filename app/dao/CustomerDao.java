package dao;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public interface CustomerDao {
    public List<Object> getAllCustomers();
    public Object getCustomerById(String customerId);
    public void addCustomer(JsonNode customer);
    public void updateCustomer(JsonNode customer);
    public void deleteCustomer(String customerId);
}
