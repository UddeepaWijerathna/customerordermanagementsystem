package dao.Impl;

import bo.Order;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import dao.OrderDao;
import db.Mongo;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

public class OrderDaoImpl implements OrderDao {
    private Mongo mongo;

    public static OrderDaoImpl orderDaoImpl;

    private OrderDaoImpl(){
        mongo = Mongo.getInstance();
    }

    public static OrderDaoImpl getInstance() {
        orderDaoImpl = orderDaoImpl == null ? new OrderDaoImpl() : orderDaoImpl;
        return orderDaoImpl;
    }

    @Override
    public List<Object> getAllOrders() {

        MongoDatabase database =  mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("orders");

        List<Object> orders = new ArrayList<>();
        for(Document order : collection.find()) {
            orders.add(order);
        }
        return orders;
    }

    @Override
    public Order getOrderById(String orderId) {
        return null;
    }

    @Override
    public void addOrder(JsonNode order) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("orders");
        DBObject dbObject = (DBObject) JSON.parse(String.valueOf(order));
        collection.insertOne(new Document((Map<String, Object>) dbObject));
    }

    @Override
    public void updateOrder(JsonNode order) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("orders");
        DBObject dbObject = (DBObject) JSON.parse(String.valueOf(order));
        collection.updateOne(eq("order_id", order.findValue("order_id")), new Document("$set", (Map<String, Object>) dbObject));
    }

    @Override
    public void deleteOrder(String orderId) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("orders");
        collection.deleteOne(eq("order_id", orderId));
    }


}
