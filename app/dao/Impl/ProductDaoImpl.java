package dao.Impl;

import bo.Product;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.util.JSON;
import dao.ProductDao;
import db.Mongo;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

public class ProductDaoImpl implements ProductDao {

    private Mongo mongo;

    public static ProductDaoImpl productDaoImpl;

    private ProductDaoImpl(){
        mongo = Mongo.getInstance();
    }

    public static ProductDaoImpl getInstance() {
        productDaoImpl = productDaoImpl == null ? new ProductDaoImpl() : productDaoImpl;
        return productDaoImpl;
    }

    @Override
    public List<Object> getAllProducts() {

        MongoDatabase database =  mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("products");

        List<Object> products = new ArrayList<>();
        for(Document product : collection.find()) {
            products.add(product);
        }
        return products;
    }

    @Override
    public Product getProductById(String productId) {
        return null;
    }

    @Override
    public void addProduct(JsonNode product) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("products");
        DBObject dbObject = (DBObject) JSON.parse(String.valueOf(product));
        collection.insertOne(new Document((Map<String, Object>) dbObject));
    }

    @Override
    public void updateProduct(JsonNode product) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("products");
        DBObject dbObject = (DBObject) JSON.parse(String.valueOf(product));
        collection.updateOne(eq("productid", product.findValue("productid")), new Document("$set", (Map<String, Object>) dbObject));
    }

    @Override
    public void deleteProduct(String productId) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("products");
        DeleteResult deleteResult = collection.deleteOne(eq("productid", productId));
    }
}
