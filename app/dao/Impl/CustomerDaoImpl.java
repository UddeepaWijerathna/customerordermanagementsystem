package dao.Impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import dao.CustomerDao;
import db.Mongo;
import org.bson.Document;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

public class CustomerDaoImpl implements CustomerDao {

    private Mongo mongo;

    public static CustomerDaoImpl customerDaoImpl;

    @Singleton
    private CustomerDaoImpl() {
        mongo = Mongo.getInstance();
    }

    public static CustomerDaoImpl getInstance() {
        customerDaoImpl = customerDaoImpl == null ? new CustomerDaoImpl() : customerDaoImpl;
        return customerDaoImpl;
    }

    @Override
    public List<Object> getAllCustomers() {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("customers");
        List<Object> customers = new ArrayList<>();
        for (Document customer : collection.find()) {
            customers.add(customer);
        }
        return customers;
    }

    @Override
    public Object getCustomerById(String customerId) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("customers");
        for (Document customer : collection.find()) {
            if (customer.get("customer_id") == customerId)
                System.out.println(customer);
            return (Object) customer;

        }
        return null;
    }

    @Override
    public void addCustomer(JsonNode customer) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("customers");
        DBObject dbObject = (DBObject) JSON.parse(String.valueOf(customer));
        collection.insertOne(new Document((Map<String, Object>) dbObject));

    }

    @Override
    public void updateCustomer(JsonNode customer) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("customers");
        DBObject dbObject = (DBObject) JSON.parse(String.valueOf(customer));
        collection.updateOne(eq("customer_id", customer.findValue("customer_id")), new Document("$set", (Map<String, Object>) dbObject));
    }

    @Override
    public void deleteCustomer(String customerId) {
        MongoDatabase database = mongo.getDB("customer_order_management_system");
        MongoCollection<Document> collection = database.getCollection("customers");
        collection.deleteOne(eq("customer_id", customerId));
    }


}
