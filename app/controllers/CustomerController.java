package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import service.Impl.CustomerServiceImpl;

import java.util.List;


public class CustomerController extends Controller {
    public play.mvc.Result getAllCustomers(){
        CustomerServiceImpl customerService= new CustomerServiceImpl();
        List<Object> customers = (List)customerService.viewCustomerList();
        return ok(Json.toJson(customers));
    }

    public play.mvc.Result getCustomerById(String customerId){
        CustomerServiceImpl customerService= new CustomerServiceImpl();
        Object customer = customerService.viewCustomerById(customerId);
        return ok(Json.toJson(customer));
    }



    public play.mvc.Result testGet() {
        ObjectNode result = Json.newObject();
        result.put("msg", "test");
        result.put("type", "testGet");
        return ok(result);
    }

    public play.mvc.Result addCustomer(Http.Request request){
        JsonNode requestData = request.body().asJson();
        CustomerServiceImpl customerService = new CustomerServiceImpl();
        customerService.addCustomer(requestData);
        return ok();
    }

    public play.mvc.Result updateCustomer(Http.Request request){
        JsonNode requestData = request.body().asJson();
        CustomerServiceImpl customerService = new CustomerServiceImpl();
        customerService.updateCustomer(requestData);
        return ok();
    }

    public play.mvc.Result deleteCustomer(String customerId){
        CustomerServiceImpl customerService = new CustomerServiceImpl();
        customerService.deleteCustomer(customerId);
        return ok();
    }

}