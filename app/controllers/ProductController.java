package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import service.Impl.ProductServiceImpl;

import java.util.List;

public class ProductController extends Controller {
    public play.mvc.Result getAllProducts(){
        ProductServiceImpl productService = new ProductServiceImpl();
        List<Object> customers = (List)productService.viewProductList();
        return ok(Json.toJson(customers));
    }

    public play.mvc.Result addProduct(Http.Request request){
        JsonNode requestData = request.body().asJson();
        ProductServiceImpl productService = new ProductServiceImpl();
        productService.addProduct(requestData);
        return ok();
    }

    public play.mvc.Result updateProduct(Http.Request request){
        JsonNode requestData = request.body().asJson();
        ProductServiceImpl productService = new ProductServiceImpl();
        productService.updateProduct(requestData);
        return ok();
    }

    public play.mvc.Result deleteProduct(String productId){
        ProductServiceImpl productService = new ProductServiceImpl();
        productService.deleteProduct(productId);
        return ok();
    }

}
