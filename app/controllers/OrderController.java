package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import service.Impl.CustomerServiceImpl;
import service.Impl.OrderServiceImpl;

import java.util.List;

public class OrderController extends Controller {
    public play.mvc.Result getAllOrders() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        List<Object> orders = (List)orderService.viewOrderList();
        return ok(Json.toJson(orders));
    }

    public play.mvc.Result addOrder(Http.Request request){
        JsonNode requestData = request.body().asJson();
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.addOrder(requestData);
        return ok();
    }
    public play.mvc.Result updateOrder(Http.Request request){
        JsonNode requestData = request.body().asJson();
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.updateOrder(requestData);
        return ok();
    }

    public play.mvc.Result deleteOrder(String orderId){
        CustomerServiceImpl customerService = new CustomerServiceImpl();
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.deleteOrder(orderId);
        return ok();
    }

}
