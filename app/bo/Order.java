package bo;

import java.util.Date;

public class Order {

    private final String orderId;
    private final String productId;
    private int quantity;
    private final String customerId;
    private final Date date;
    private String orderStatus;

    public Order(String orderId, String productId, int quantity, String customerId, Date date, String orderStatus) {
        this.orderId = orderId;
        this.productId = productId;
        this.quantity = quantity;
        this.customerId = customerId;
        this.date = date;
        this.orderStatus = orderStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getCustomerId() {
        return customerId;
    }

    public Date getDate() {
        return date;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
