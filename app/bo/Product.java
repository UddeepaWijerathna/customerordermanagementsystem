package bo;

public class Product {

    private final String productId;
    private final String productName ;
    private  int stockQuantity;
    private  double pricePerUnit;

    public Product(String productId, String productName, int stockQuantity, double pricePerUnit) {
        this.productId = productId;
        this.productName = productName;
        this.stockQuantity = stockQuantity;
        this.pricePerUnit = pricePerUnit;
    }

    public String getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public void setPricePerUnit(double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }
}
