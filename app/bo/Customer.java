package bo;



public class Customer{
    private String customerId;
    private String name;
    private Address address;
    private int age;
    private boolean activeStatus;

    public Customer(String customerId, String name, Address address, int age, boolean activeStatus) {
        this.customerId = customerId;
        this.name = name;
        this.address=address;
        this.age = age;
        this.activeStatus = activeStatus;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public int getAge() {
        return age;
    }

    public boolean isActiveStatus() {
        return activeStatus;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setActiveStatus(boolean activeStatus) {
        this.activeStatus = activeStatus;
    }
}
