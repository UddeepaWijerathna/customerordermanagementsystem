package bo;
public class Address {
    private String addressLine1;
    private String addressLine2;
    private int streetNo;
    private String landmark;
    private String city;
    private String country;
    private int zipCode;

    public Address(String addressLine1, String addressLine2, int streetNo, String landmark, String city, String country, int zipCode) {
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.streetNo = streetNo;
        this.landmark = landmark;
        this.city = city;
        this.country = country;
        this.zipCode = zipCode;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public int getStreetNo() {
        return streetNo;
    }

    public String getLandmark() {
        return landmark;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public void setStreetNo(int streetNo) {
        this.streetNo = streetNo;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }
}
