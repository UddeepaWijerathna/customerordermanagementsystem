package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface ProductService {
    public Object viewProductList();
    public void addProduct(JsonNode product);
    public void updateProduct(JsonNode product);
    public void deleteProduct(String productId);
}
