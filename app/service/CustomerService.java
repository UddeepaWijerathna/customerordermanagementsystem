package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface CustomerService {
    public Object viewCustomerList();
    public Object viewCustomerById(String customerId);
    public void addCustomer(JsonNode customer);
    public void updateCustomer(JsonNode customer);
    public void deleteCustomer(String customerId);

}
