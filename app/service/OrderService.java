package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface OrderService {
    public Object viewOrderList();
    public void addOrder(JsonNode order);
    public void updateOrder(JsonNode order);
    public void deleteOrder(String orderId);
    public boolean isActiveCustomer(String customerId);
}
