package service.Impl;

import com.fasterxml.jackson.databind.JsonNode;
import dao.Impl.CustomerDaoImpl;
import service.CustomerService;

import java.util.logging.Logger;

public class CustomerServiceImpl implements CustomerService {

    CustomerDaoImpl customerDaoImpl;
    private static Logger logger;

    public CustomerServiceImpl() {
        customerDaoImpl = CustomerDaoImpl.getInstance();
        logger = Logger.getLogger(String.valueOf(CustomerDaoImpl.class));
    }

    @Override
    public Object viewCustomerList() {
        return customerDaoImpl.getAllCustomers();
    }

    @Override
    public Object viewCustomerById(String customerId) {
        return customerDaoImpl.getCustomerById(customerId);
    }

    @Override
    public void addCustomer(JsonNode customer) {
        logger.info("adding successful");
        customerDaoImpl.addCustomer(customer);
    }

    @Override
    public void updateCustomer(JsonNode customer) {
        logger.info("updating successful");
        customerDaoImpl.updateCustomer(customer);
    }

    @Override
    public void deleteCustomer(String customerId) {
        //logger.info(customerId);
        customerDaoImpl.deleteCustomer(customerId);
    }

}
