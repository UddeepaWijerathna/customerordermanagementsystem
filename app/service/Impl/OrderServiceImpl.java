package service.Impl;

import com.fasterxml.jackson.databind.JsonNode;
import dao.Impl.OrderDaoImpl;
import service.OrderService;

public class OrderServiceImpl implements OrderService {

    OrderDaoImpl orderDaoImpl;

    public OrderServiceImpl(){
        orderDaoImpl = OrderDaoImpl.getInstance();
    }

    @Override
    public Object viewOrderList() {
        return orderDaoImpl.getAllOrders();
    }

    @Override
    public void addOrder(JsonNode order) {
        orderDaoImpl.addOrder(order);
    }

    @Override
    public void updateOrder(JsonNode order) {
        orderDaoImpl.updateOrder(order);
    }

    @Override
    public void deleteOrder(String orderId) {
        orderDaoImpl.deleteOrder(orderId);
    }

    public boolean isActiveCustomer(String customerId){
      return true;
    }

}
