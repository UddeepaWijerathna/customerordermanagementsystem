package service.Impl;

import com.fasterxml.jackson.databind.JsonNode;
import dao.Impl.ProductDaoImpl;
import service.ProductService;

public class ProductServiceImpl implements ProductService {
    ProductDaoImpl productDaoImpl;

    public ProductServiceImpl() {
        productDaoImpl = ProductDaoImpl.getInstance();
    }

    @Override
    public Object viewProductList() {
        return productDaoImpl.getAllProducts();
    }

    @Override
    public void addProduct(JsonNode product) {
        productDaoImpl.addProduct(product);
    }

    @Override
    public void deleteProduct(String productId) {
        productDaoImpl.deleteProduct(productId);
    }
    @Override
    public void updateProduct(JsonNode product) {
        productDaoImpl.updateProduct(product);
    }
}
