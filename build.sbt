name := """CustomerOrderManagementSystem"""
organization := "com.nCinga"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"
libraryDependencies += "com.google.code" % "morphia" % "0.91"
libraryDependencies += "log4j" % "log4j" % "1.2.17"

libraryDependencies ++= Seq(
  guice,
  "org.pac4j" % "pac4j-mongo" % "3.8.3",
  "org.mongodb" % "mongo-java-driver" % "3.8.2"
)
